import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.fail;

public class TestUrl {


    @Test
    public void testHTTPSProtocol() {
        Url testUrl = new Url("https://www.launchcode.com/test");
        assertEquals("https", testUrl.getProtocol());
    }

    @Test
    public void testFTPProtocol() {
        Url testUrl = new Url("ftp://www.launchcode.com/test");
        assertEquals("ftp", testUrl.getProtocol());
    }

    @Test
    public void testFILEProtocol() {
        Url testUrl = new Url("file://www.launchcode.com/test");
        assertEquals("file", testUrl.getProtocol());
    }

    @Test
    public void testDomain() {
        Url testUrl = new Url("http://www.launchcode.com/test");
        assertEquals("www.launchcode.com", testUrl.getDomain());
    }

    @Test
    public void testDomainNoSubDomain() {
        Url testUrl = new Url("http://launchcode.com/test");
        assertEquals("launchcode.com", testUrl.getDomain());
    }

    @Test
    public void testPath() {
        Url testUrl = new Url("http://www.launchcode.com/test");
        assertEquals("test", testUrl.getPath());
    }


    @Test
    public void testUrlToString() {
        Url testUrl = new Url("http://www.launchcode.com/test");
        assertEquals("http://www.launchcode.com/test", testUrl.toString());
    }


}
