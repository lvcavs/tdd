

public class Url {
    private final String protocol;
    private final String domain;
    private final String path;
    private final String subDomain;

    public Url(String url) {
        this.protocol = setProtocol(url);
        this.domain = setDomain(url);
        this.path = setPath(url);
        this.subDomain = "www.";

    }

    private String setProtocol(String url) {
        String[] splitUrl = url.split(":");
        String protocol = splitUrl[0];
        return protocol.toLowerCase();
    }

    public String getProtocol() {
        return this.protocol;
    }

    private String setDomain(String url) {
        String[] splitUrl = url.split("://");
        String domain = splitUrl[1];
        return domain.toLowerCase();
    }

    public String getDomain() {
        return this.domain;
    }

    private String setPath(String url) {
        String[] splitUrl = url.split("://");
        String path = "";
        if(splitUrl[1].indexOf('/') != -1) {
            path = splitUrl[1].substring(splitUrl[1].indexOf('/') + 1);
        }
        return path.toLowerCase();
    }

    public String getPath() {
        return this.path;
    }

    public String toString() {
        String subDomain = "www.";


        return this.protocol + "://" + subDomain + this.domain + "/" + this.path;
    }
}
